# Guía para escribir documentación técnica con DocBook XML

## Introducción

Este documento contiene las siguientes secciones:

 * Organización del libro.
 * Como trabajar con los capítulos.
 * Sobre el documento bookinfo.
 * Como generar alchivos en diferentes formato

## Organización del libro

El libro esta organizado en varios archivos, un archivo maestro que contiene
la estructura básica del documento y archivos individuales para los diferentes
capítulos.

El documento maestro se llama index.xml, el cual tiene los siguientes
elementos:
- Declaración de documento en XML.
- Declaración de el Tipo de Documento (DTD).
- Elemento raíz del documento <book>

Dentro de el elemento raíz se hacen las referencias a los capítulos. Cada
capitulo esta en un archivo individual dentro del mismo directorio.
Normalmente los archivos que editaremos para agregar contenido serán los
archivos de los capítulos, vea los capitulos de ejemplo.

## Como trabajar con los capítulos

Para agregar contenido en los archivos de los capítulos, se tendrá que modificar
con cualquier editor de texto, nosotros recomendamos Vim.

## Sobre el documento bookinfo.

El archivo bookinfo.xml contiene la información referente al libro, como:
los autores, fechas de publicación, revisiones del documento, etc.
Este documento solo se deberá de modificar cuando se requiera agregar
a algún autor o agregar una revisión del documento.

## Como generar alchivos en diferentes formato

TODO.
