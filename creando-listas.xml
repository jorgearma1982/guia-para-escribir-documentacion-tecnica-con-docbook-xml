<?xml version="1.0" encoding="UTF-8"?>
  <chapter id="creando-listas">
    <title>Creando Listas</title>

    <para>En ciertas ocaciones vamos a requerir enlistar algunos elementos, y
    para esto utilizaremos las etiquetas de bloque para crear listas, a
    continuacion se veran dos tipos de listas que podemos usar con
    DocBook.</para>

    <para>Las listas estan compuestas de elementos, cada elemento de una lista
    puede contener a su vez otra lista y tambien parrafos. abajo veremos unos
    ejemplos.</para>

    <section id="simplelist">
      <title>Simplelist (lista simple)</title>

      <para>Esta es una de las listas con un formato de lo mas simple. La
      <ulink
      url="http://www.docbook.org/tdg/en/html/simplelist.html">simplelist</ulink>
      esta diseñada para listar frases cortas (como una lista de frutas) y
      solo requiere de dos etiquetas para construir la lista como se muestra
      en el ejemplo de abajo</para>

      <para>Elementos de simple list</para>

      <variablelist>
        <varlistentry>
          <term>&lt;simplelist&gt;</term>

          <listitem>
            <para>Lista de palabras o frases cortas (elementos)</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>&lt;member&gt;</term>

          <listitem>
            <para>Miembro o elemento de una lista simple (simplelist), esta
            etiqueta solo puede contener contenido inline, por lo que una
            simplelist no puede contener otras listas.</para>
          </listitem>
        </varlistentry>
      </variablelist>

      <para><example>
          <title>Una lista simplelist</title>

          <screen>&lt;simplelist&gt;
  &lt;member&gt;&lt;emphasis role="bold"&gt;Manzanas&lt;/emphasis&gt;&lt;/member&gt;
  &lt;member&gt;&lt;emphasis role="bold"&gt;Naranjas&lt;/emphasis&gt;&lt;/member&gt;
  &lt;member&gt;&lt;emphasis role="bold"&gt;Platanos&lt;/emphasis&gt;&lt;/member&gt;
  &lt;member&gt;&lt;emphasis role="bold"&gt;Uvas&lt;/emphasis&gt;&lt;/member&gt;
  &lt;member&gt;&lt;emphasis role="bold"&gt;Melones&lt;/emphasis&gt;&lt;/member&gt;
&lt;/simplelist&gt;</screen>
        </example></para>

      <para>Cuando esta lista es convertida, la lista simple (simplelist) se
      veria asi:</para>

      <simplelist>
        <member>Manzanas</member>

        <member>Naranjas</member>

        <member>Platanos</member>

        <member>Uvas</member>

        <member>Melones</member>
      </simplelist>

      <para></para>
    </section>

    <section id="itemizedlist">
      <title>Itemizedlist</title>

      <para>Una lista <ulink
      url="http://www.docbook.org/tdg/en/html/itemizedlist.html">itemizedlist</ulink>
      es similar a la lista simple excepto que cada entrada contiene un
      parrafo en lugar de una frase corta, esto te permite agregar contenido
      más variado en tu lista. Las listas itemizedlist pueden contener otras
      listas.</para>

      <para>Los elementos que conforman una itemizedlist, son:</para>

      <variablelist>
        <varlistentry>
          <term>&lt;itemizedlist&gt;</term>

          <listitem>
            <para>Lista en la cual cada entrada esta marcada con una viñeta,
            guion, u algun otro ornamento de impresion (dingbat).</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>&lt;listitem&gt;</term>

          <listitem>
            <para>Cada uno de los elementos en una lista itemizedlist o
            orderedlist</para>
          </listitem>
        </varlistentry>
      </variablelist>

      <para><example>
          <title>Una lista itemizedlist</title>

          <screen>&lt;itemizedlist&gt;
  &lt;listitem&gt;
    &lt;para&gt;Elemento 1 - Primero de la lista.&lt;/para&gt;
  &lt;/listitem&gt;
  &lt;listitem&gt;
    &lt;para&gt;Elemento 2 - Segundo de la lista.&lt;/para&gt;
  &lt;/listitem&gt;
  &lt;listitem&gt;
    &lt;para&gt;Elemento 3 - Tercer de la lista.&lt;/para&gt;
  &lt;/listitem&gt;
  &lt;listitem&gt;
    &lt;para&gt;Elemento 4 - Cuarto de la lista.&lt;/para&gt;
  &lt;/listitem&gt;
  &lt;listitem&gt;
    &lt;para&gt;Elemento 5 - Quinto de la lista.&lt;/para&gt;
  &lt;/listitem&gt;
&lt;/itemizedlist&gt;</screen>
        </example></para>

      <para>Una vez convertido, una itemizedlist se veria asi:</para>

      <itemizedlist>
        <listitem>
          <para>Elemento 1 - Primero de la lista.</para>
        </listitem>

        <listitem>
          <para>Elemento 2 - Segundo de la lista.</para>
        </listitem>

        <listitem>
          <para>Elemento 3 - Tercer de la lista.</para>
        </listitem>

        <listitem>
          <para>Elemento 4 - Cuarto de la lista.</para>
        </listitem>

        <listitem>
          <para>Elemento 5 - Quinto de la lista.</para>
        </listitem>
      </itemizedlist>

      <para>Usaremos este tipo de lista cuando el orden de los elementos no
      tiene tanta importancia, por ejemplo, solo estamos listando elementos, y
      no hay ningun orden a seguir de importancia.</para>

      <para><example>
          <title>Una lista itemizedlist con listas anidadas</title>

          <screen>&lt;itemizedlist&gt;
  &lt;listitem&gt;
    &lt;para&gt;Elemento de lista 1&lt;/para&gt;
  &lt;/listitem&gt;
        
  &lt;listitem&gt;
    &lt;para&gt;Elemento de lista 2&lt;/para&gt;
        
    &lt;para&gt;Parrafo de texto dentro de un elemento de lista.&lt;/para&gt;
  &lt;/listitem&gt;
        
  &lt;listitem&gt;
    &lt;para&gt;Elemento de lista 3&lt;/para&gt;
        
    &lt;para&gt;Parrafo de texto dentro de el elemento 3;&lt;/para&gt;
        
    &lt;para&gt;Siguen más listas dentro de un elemento de lista&lt;/para&gt;
        
    &lt;itemizedlist&gt;
      &lt;listitem&gt;
        &lt;para&gt;Elemento de lista 3.1&lt;/para&gt;
      &lt;/listitem&gt;
        
      &lt;listitem&gt;
        &lt;para&gt;Elemento de lista 3.2&lt;/para&gt;
      &lt;/listitem&gt;
    &lt;/itemizedlist&gt;
        
    &lt;para&gt;Parrafo de texto para elemento 3;&lt;/para&gt;
        
  &lt;/listitem&gt;
        
  &lt;listitem&gt;
    &lt;para&gt;Elemento de lista 4&lt;/para&gt;
  &lt;/listitem&gt;
&lt;/itemizedlist&gt;</screen>
        </example></para>

      <para>El ejemplo ya convertido se veria asi:</para>

      <itemizedlist>
        <listitem>
          <para>Elemento de lista 1</para>
        </listitem>

        <listitem>
          <para>Elemento de lista 2</para>

          <para>Parrafo de texto dentro de un elemento de lista.</para>
        </listitem>

        <listitem>
          <para>Elemento de lista 3</para>

          <para>Parrafo de texto dentro de el elemento 3;</para>

          <para>Siguen más listas dentro de un elemento de lista</para>

          <itemizedlist>
            <listitem>
              <para>Elemento de lista <emphasis
              role="bold">3.1</emphasis></para>
            </listitem>

            <listitem>
              <para>Elemento de lista <emphasis
              role="bold">3.2</emphasis></para>
            </listitem>
          </itemizedlist>

          <para>Parrafo de texto para elemento 3;</para>
        </listitem>

        <listitem>
          <para>Elemento de lista 4</para>
        </listitem>
      </itemizedlist>

      <para></para>
    </section>

    <section id="orderedlist">
      <title>Orderedlist</title>

      <para>La lista <ulink
      url="http://www.docbook.org/tdg/en/html/orderedlist.html">orderedlist</ulink>
      es como la lista itemizedlist excepto que cada listitem esta precedido
      por un numero o letra en lugar de una viñeta. Las orderedlist pueden
      contenter otras listas anidadas</para>

      <para>El atributo «numeration» especifica que tipo de numeracion sera
      usada y puede ser uno de los siguientes valores:</para>

      <variablelist>
        <varlistentry>
          <term>arabic</term>

          <listitem>
            <para>La númeracion sera con números arábigos, por ejemplo: 1, 2,
            3, 4, 5 ,6, 7, 8 ,9</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>upperalpha</term>

          <listitem>
            <para>Letras en mayusculas</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>loweralpha</term>

          <listitem>
            <para>Letras en minusculas</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>upperroman</term>

          <listitem>
            <para>Números romanos en mayusculas</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>lowerroman</term>

          <listitem>
            <para>Números romanos en minusculas</para>
          </listitem>
        </varlistentry>
      </variablelist>

      <para>Este tipo de lista lo usaremos cuando el orden de los elementos si
      importa, por ejemplo aqui si podemos listar los elementos en orden de
      importancia.</para>

      <para><example>
          <title>Una lista orderedlist</title>

          <screen>&lt;orderedlist numeration="arabic"&gt;
  &lt;listitem&gt;
    &lt;para&gt;Levantarse.&lt;/para&gt;
  &lt;/listitem&gt;

  &lt;listitem&gt;
    &lt;para&gt;Desayunar.&lt;/para&gt;
  &lt;/listitem&gt;

  &lt;listitem&gt;
    &lt;para&gt;Tomar un baño.&lt;/para&gt;
  &lt;/listitem&gt;

  &lt;listitem&gt;
    &lt;para&gt;Tomar transporte al trabajo.&lt;/para&gt;
  &lt;/listitem&gt;

  &lt;listitem&gt;
    &lt;para&gt;Labores de trabajo.&lt;/para&gt;
  &lt;/listitem&gt;

  &lt;listitem&gt;
    &lt;para&gt;Tomar transporte de regreso a casa.&lt;/para&gt;
  &lt;/listitem&gt;

  &lt;listitem&gt;
    &lt;para&gt;Cenar.&lt;/para&gt;
  &lt;/listitem&gt;

  &lt;listitem&gt;
    &lt;para&gt;Ir a dormir.&lt;/para&gt;
  &lt;/listitem&gt;
&lt;/orderedlist&gt;</screen>
        </example></para>

      <para>Convertida, esta lista se veria asi:</para>

      <orderedlist numeration="arabic">
        <listitem>
          <para>Levantarse.</para>
        </listitem>

        <listitem>
          <para>Desayunar.</para>
        </listitem>

        <listitem>
          <para>Tomar un baño.</para>
        </listitem>

        <listitem>
          <para>Tomar transporte al trabajo.</para>
        </listitem>

        <listitem>
          <para>Labores de trabajo.</para>
        </listitem>

        <listitem>
          <para>Tomar transporte de regreso a casa.</para>
        </listitem>

        <listitem>
          <para>Cenar.</para>
        </listitem>

        <listitem>
          <para>Ir a dormir.</para>
        </listitem>
      </orderedlist>

      <para><example>
          <title>orderedlist con otras listas anidadas y diferente formato de
          numeración</title>

          <screen>&lt;orderedlist numeration="arabic"&gt;
  &lt;listitem&gt;
    &lt;para&gt;Levantarse.&lt;/para&gt;
  &lt;/listitem&gt;

  &lt;listitem&gt;
    &lt;para&gt;Desayunar.&lt;/para&gt;
  &lt;/listitem&gt;

  &lt;listitem&gt;
    &lt;para&gt;Tomar un baño.&lt;/para&gt;
  &lt;/listitem&gt;

  &lt;listitem&gt;
    &lt;para&gt;Tomar transporte al trabajo.&lt;/para&gt;
  &lt;/listitem&gt;

  &lt;listitem&gt;
    &lt;para&gt;Labores de trabajo.&lt;/para&gt;
        
    &lt;orderedlist numeration="lowerroman"&gt;
      &lt;listitem&gt;
        &lt;para&gt;Revisar reportes.&lt;/para&gt;
      &lt;/listitem&gt;

      &lt;listitem&gt;
        &lt;para&gt;Enviar reportes.&lt;/para&gt;
      &lt;/listitem&gt;

      &lt;listitem&gt;
        &lt;para&gt;Chatiar.&lt;/para&gt;
      &lt;/listitem&gt;

    &lt;/orderedlist&gt;

  &lt;/listitem&gt;
        
  &lt;listitem&gt;
    &lt;para&gt;Tomar transporte de regreso a casa.&lt;/para&gt;
  &lt;/listitem&gt;
        
  &lt;listitem&gt;
    &lt;para&gt;Cenar.&lt;/para&gt;
  &lt;/listitem&gt;

  &lt;listitem&gt;
    &lt;para&gt;Ir a dormir.&lt;/para&gt;
  &lt;/listitem&gt;
&lt;/orderedlist&gt;</screen>
        </example></para>

      <para>Una vez convertido, la lista se veria asi:</para>

      <orderedlist numeration="arabic">
        <listitem>
          <para>Levantarse.</para>
        </listitem>

        <listitem>
          <para>Desayunar.</para>
        </listitem>

        <listitem>
          <para>Tomar un baño.</para>
        </listitem>

        <listitem>
          <para>Tomar transporte al trabajo.</para>
        </listitem>

        <listitem>
          <para>Labores de trabajo.</para>

          <orderedlist numeration="lowerroman">
            <listitem>
              <para>Revisar reportes.</para>
            </listitem>

            <listitem>
              <para>Enviar reportes.</para>
            </listitem>

            <listitem>
              <para>Chatiar.</para>
            </listitem>
          </orderedlist>
        </listitem>

        <listitem>
          <para>Tomar transporte de regreso a casa.</para>
        </listitem>

        <listitem>
          <para>Cenar.</para>
        </listitem>

        <listitem>
          <para>Ir a dormir.</para>
        </listitem>
      </orderedlist>

      <para></para>
    </section>

    <section id="variablelist">
      <title>variablelist</title>

      <para>Este tipo de lista es usado cuando tenemos una lista de terminos y
      definiciones. La <ulink
      url="http://www.docbook.org/tdg/en/html/variablelist.html">variablelist</ulink>
      consiste en varias etiquetas, las cuelas son:</para>

      <variablelist>
        <varlistentry>
          <term>&lt;varlistentry&gt;</term>

          <listitem>
            <para>La cual es usada para agrugar terminos relacionados</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>&lt;term&gt;</term>

          <listitem>
            <para>Esta etiqueta contiene el termino</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>&lt;listitem&gt;</term>

          <listitem>
            <para>Esta etiqueta contendra la descripción de el termino.</para>
          </listitem>
        </varlistentry>
      </variablelist>

      <para><example>
          <title>Una lista variablelist</title>

          <screen>&lt;variablelist&gt;
  &lt;varlistentry&gt;
    &lt;term&gt;arabic&lt;/term&gt;

    &lt;listitem&gt;
      &lt;para&gt;La númeracion sera con números arábigos, por ejemplo: 1, 2, 3, 4, 5 ,6, 7, 8 ,9&lt;/para&gt;
    &lt;/listitem&gt;
  &lt;/varlistentry&gt;

  &lt;varlistentry&gt;
    &lt;term&gt;upperalpha&lt;/term&gt;

    &lt;listitem&gt;
      &lt;para&gt;Letras en mayusculas&lt;/para&gt;
    &lt;/listitem&gt;
  &lt;/varlistentry&gt;

  &lt;varlistentry&gt;
    &lt;term&gt;loweralpha&lt;/term&gt;

    &lt;listitem&gt;
      &lt;para&gt;Letras en minusculas&lt;/para&gt;
    &lt;/listitem&gt;
  &lt;/varlistentry&gt;

  &lt;varlistentry&gt;
    &lt;term&gt;upperroman&lt;/term&gt;

    &lt;listitem&gt;
      &lt;para&gt;Números romanos en mayusculas&lt;/para&gt;
    &lt;/listitem&gt;
  &lt;/varlistentry&gt;

  &lt;varlistentry&gt;
    &lt;term&gt;lowerroman&lt;/term&gt;

    &lt;listitem&gt;
      &lt;para&gt;Números romanos en minusculas&lt;/para&gt;
    &lt;/listitem&gt;
  &lt;/varlistentry&gt;
&lt;/variablelist&gt;</screen>
        </example></para>

      <para>Una vez convertida esta lista se veria así:</para>

      <variablelist>
        <varlistentry>
          <term>arabic</term>

          <listitem>
            <para>La númeracion sera con números arábigos, por ejemplo: 1, 2,
            3, 4, 5 ,6, 7, 8 ,9</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>upperalpha</term>

          <listitem>
            <para>Letras en mayusculas</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>loweralpha</term>

          <listitem>
            <para>Letras en minusculas</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>upperroman</term>

          <listitem>
            <para>Números romanos en mayusculas</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>lowerroman</term>

          <listitem>
            <para>Números romanos en minusculas</para>
          </listitem>
        </varlistentry>
      </variablelist>

      <para>Esta será una de las listas mas usadas cuando se definen opciones
      y parametros de programas o archivos de configuración.</para>
    </section>

    <section id="segmentedlist">
      <title>Segmentedlist</title>

      <para>Las listas <ulink
      url="http://www.docbook.org/tdg/en/html/segmentedlist.html">segmentedlist</ulink>
      son usadas para listar informacion en distintos campos como el contenido
      de una libreta de direcciones.</para>

      <para>Las etiquetas utilizadas son:</para>

      <variablelist>
        <varlistentry>
          <term>&lt;segtitle&gt;</term>

          <listitem>
            <para>Aqui contendra el nombre de cada campo</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>&lt;seglistitem&gt;</term>

          <listitem>
            <para>Inicia cada grupo de datos</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>&lt;seg&gt;</term>

          <listitem>
            <para>Los satos se ponen dentro de estas etiquetas</para>
          </listitem>
        </varlistentry>
      </variablelist>

      <para><example>
          <title>Una lista segmentedlist con formato «lista»</title>

          <screen>&lt;para&gt;
Las capitales de los estados de México son:

&lt;segmentedlist&gt;
  &lt;title&gt;Capitales de los estados&lt;/title&gt;
  &lt;?dbhtml list-presentation="list"?&gt;
  &lt;segtitle&gt;Estado&lt;/segtitle&gt;
  &lt;segtitle&gt;Capital&lt;/segtitle&gt;
  &lt;seglistitem&gt;
    &lt;seg&gt;Jalisco&lt;/seg&gt;
    &lt;seg&gt;Guadalajara&lt;/seg&gt;
  &lt;/seglistitem&gt;
  &lt;seglistitem&gt;
    &lt;seg&gt;Baja California&lt;/seg&gt;
    &lt;seg&gt;Mexicali&lt;/seg&gt;
  &lt;/seglistitem&gt;
  &lt;seglistitem&gt;
    &lt;seg&gt;Guanajuato&lt;/seg&gt;
    &lt;seg&gt;Guanajuato&lt;/seg&gt;
  &lt;/seglistitem&gt;
&lt;/segmentedlist&gt;

&amp;hellip;
&lt;/para&gt;</screen>
        </example></para>

      <para>Convertida se veria así:</para>

      <para>Las capitales de los estados de México son: <segmentedlist>
          <title>Capitales de los estados</title>

          <?dbhtml list-presentation="list"?>

          <segtitle>Estado</segtitle>

          <segtitle>Capital</segtitle>

          <seglistitem>
            <seg>Jalisco</seg>

            <seg>Guadalajara</seg>
          </seglistitem>

          <seglistitem>
            <seg>Baja California</seg>

            <seg>Mexicali</seg>
          </seglistitem>

          <seglistitem>
            <seg>Guanajuato</seg>

            <seg>Guanajuato</seg>
          </seglistitem>
        </segmentedlist> …</para>

      <para><example>
          <title>Una lista segmentedlist en formato «table»</title>

          <screen>&lt;para&gt;
Las capitales de los estados de México son:

&lt;segmentedlist&gt;
  &lt;title&gt;Capitales de los estados&lt;/title&gt;
  &lt;?dbhtml list-presentation="table"?&gt;
  &lt;segtitle&gt;Estado&lt;/segtitle&gt;
  &lt;segtitle&gt;Capital&lt;/segtitle&gt;
  &lt;seglistitem&gt;
    &lt;seg&gt;Jalisco&lt;/seg&gt;
    &lt;seg&gt;Guadalajara&lt;/seg&gt;
  &lt;/seglistitem&gt;
  &lt;seglistitem&gt;
    &lt;seg&gt;Baja California&lt;/seg&gt;
    &lt;seg&gt;Mexicali&lt;/seg&gt;
  &lt;/seglistitem&gt;
  &lt;seglistitem&gt;
    &lt;seg&gt;Guanajuato&lt;/seg&gt;
    &lt;seg&gt;Guanajuato&lt;/seg&gt;
  &lt;/seglistitem&gt;
&lt;/segmentedlist&gt;

&amp;hellip;
&lt;/para&gt;</screen>
        </example></para>

      <para>Convertida se veria asi:</para>

      <para>Las capitales de los estados de México son: <segmentedlist>
          <title>Capitales de los estados</title>

          <?dbhtml list-presentation="table"?>

          <segtitle>Estado</segtitle>

          <segtitle>Capital</segtitle>

          <seglistitem>
            <seg>Jalisco</seg>

            <seg>Guadalajara</seg>
          </seglistitem>

          <seglistitem>
            <seg>Baja California</seg>

            <seg>Mexicali</seg>
          </seglistitem>

          <seglistitem>
            <seg>Guanajuato</seg>

            <seg>Guanajuato</seg>
          </seglistitem>
        </segmentedlist> …</para>

      <para></para>
    </section>

    <section id="qandaset">
      <title>qandaset</title>

      <para>La lista <ulink
      url="http://www.docbook.org/tdg/en/html/qandaset.html">qandaset</ulink>
      es una lista especialmente diseñada para tratar con grupos de preguntas
      y respuestas, como normalmente uno las ve en un FAQ.</para>

      <para>Etiquetas que conforman una lista qandaset:</para>

      <variablelist>
        <varlistentry>
          <term>&lt;qandaentry&gt;</term>

          <listitem>
            <para>Cada grupo de preguntas y respuestas estan contenidas dentro
            de esta etiqueta.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>&lt;question&gt;</term>

          <listitem>
            <para>Aqui es donde va la pregunta.</para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>&lt;answer&gt;</term>

          <listitem>
            <para>Y obvio aqui la respuesta.</para>
          </listitem>
        </varlistentry>
      </variablelist>

      <para><example>
          <title>Una lista qandaset</title>

          <screen>&lt;qandaset defaultlabel='qanda'&gt;
  &lt;qandaentry&gt;
    &lt;question&gt;
      &lt;para&gt;Ser, o no ser?&lt;/para&gt;
    &lt;/question&gt;
    &lt;answer&gt;
      &lt;para&gt;Esa es la pregunta.&lt;/para&gt;
    &lt;/answer&gt;
  &lt;/qandaentry&gt;
&lt;/qandaset&gt;                   </screen>
        </example></para>

      <para>Una vez convertido se veria asi:</para>

      <qandaset defaultlabel="qanda">
        <qandaentry>
          <question>
            <para>Ser, o no ser?</para>
          </question>

          <answer>
            <para>Esa es la pregunta.</para>
          </answer>
        </qandaentry>
      </qandaset>

      <para></para>
    </section>

    <section id="procedures">
      <title>Procedures</title>

      <para>Las listas <ulink
      url="http://www.docbook.org/tdg/en/html/procedure.html">procedure</ulink>
      son un tipo de orderedlist especializados para listar procedimientos
      paso-a-paso como normalmente los vemos en una receta o en un HowTo de
      Linux.</para>

      <para><example>
          <title>Una lista procedure</title>

          <screen>&lt;procedure&gt;&lt;title&gt;Un ejemplo de procedimiento&lt;/title&gt;
&lt;step&gt;
  &lt;para&gt;
    Un paso
  &lt;/para&gt;
&lt;/step&gt;
&lt;step&gt;
  &lt;para&gt;
    Otro paso
  &lt;/para&gt;
  &lt;substeps&gt;
    &lt;step&gt;
      &lt;para&gt;
        Sub-pasos pueden ser anidados indefinidamente.
      &lt;/para&gt;
    &lt;/step&gt;
  &lt;/substeps&gt;
&lt;/step&gt;
&lt;step&gt;
  &lt;para&gt;
    Un paso final
  &lt;/para&gt;
&lt;/step&gt;
&lt;/procedure&gt;         </screen>
        </example></para>

      <para>Una vez convertido se veria así</para>

      <procedure>
        <title>Un ejemplo de procedimiento</title>

        <step>
          <para>Un paso</para>
        </step>

        <step>
          <para>Otro paso</para>

          <substeps>
            <step>
              <para>Sub-pasos pueden ser anidados indefinidamente.</para>
            </step>
          </substeps>
        </step>

        <step>
          <para>Un paso final</para>
        </step>
      </procedure>

      <para></para>
    </section>

    <section id="listas-combinadas">
      <title>Listas combinadas</title>

      <para>Tambien podemos combinar algunas de las listas antes
      mencionadas.</para>

      <para></para>
    </section>
  </chapter>
